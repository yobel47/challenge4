import PushNotification from 'react-native-push-notification';
import { Platform } from 'react-native';

export const configure = () => {
  PushNotification.configure({
    onRegister(token) {
      console.log('TOKEN:', token);
    },

    onNotification(notification) {
      console.log('NOTIFICATION:', notification);
    },

    requestPermissions: Platform.OS === 'ios',
  });
};

export const createChannel = (channel) => {
  PushNotification.createChannel({
    channelId: channel, // (required)
    channelName: 'My channel', // (required)
  });
};

export const sendNotification = (channel, title, message) => {
  PushNotification.localNotification({
    channelId: channel, // (required)
    title, // (optional)
    message, // (required)
  });
};
