import { StyleSheet, Dimensions } from 'react-native';
import { thirdColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    width: window.width,
    height: window.height,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  logoContainer: {
    width: window.width,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 9,
  },
  logo: {
    width: window.width * 0.9,
    height: window.width * 0.9,
  },
  logoText: {
    fontSize: 48,
    marginTop: -window.width * 0.1,
    color: 'black',
    fontFamily: 'Poppins-ExtraBold',
  },
  ccContainer: {
    width: window.width,
    alignItems: 'center',
    flex: 1,
  },
  ccBackground: {
    backgroundColor: thirdColor,
    width: window.width * 0.5,
    height: window.width * 0.5,
    borderRadius: 100,
    alignItems: 'center',
  },
  ccText: {
    color: 'black',
    fontSize: 24,
    marginTop: 2,
    letterSpacing: 4,
    fontFamily: 'Poppins-ExtraBold',
  },
});

export default styles;
