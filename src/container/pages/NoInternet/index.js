import {
  StyleSheet, Text, View, Dimensions, StatusBar,
} from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';
import { BadNetworkImage } from '../../assets';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  imageContainer: {
    width: window.width,
    height: window.height,
    justifyContent: 'center',
  },
  image: {
    width: window.width,
    marginTop: -window.width * 0.1,
  },
  imageText: {
    textAlign: 'center',
    fontSize: 42,
    marginTop: -window.width * 0.35,
    lineHeight: 45,
    color: primaryColor,
    fontFamily: 'Poppins-ExtraBold',
  },
});

function NoInternet() {
  return (
    <View>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      <View style={styles.imageContainer}>
        <LottieView
          source={BadNetworkImage}
          style={styles.image}
          resizeMode="cover"
          autoPlay
          loop
        />
        <Text style={styles.imageText}>Lost Internet Connection</Text>
      </View>
    </View>
  );
}

export default NoInternet;
