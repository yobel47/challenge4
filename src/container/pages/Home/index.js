import {
  View, FlatList, Text, StatusBar,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import React, { useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { Button } from 'react-native-paper';
import { BookCarousel, Title, BookCard } from '../../components';
import styles from './styles';
import { primaryColor, secondaryColor } from '../../utils/colors';
import { LoadingImage } from '../../assets';
import { getData, setLoading, setLogin } from '../../../redux/actions';

function Home({ navigation }) {
  const isLoading = useSelector((state) => state.app.isLoading);
  const userData = useSelector((state) => state.login.userData);
  const bookData = useSelector((state) => state.data.bookData);
  const recommendedData = bookData
    .slice()
    .sort((a, b) => b.average_rating - a.average_rating)
    .slice(0, 6);

  const dispatch = useDispatch();
  const getBookData = (token) => dispatch(getData(token));
  const actionSetLoading = (loading) => dispatch(setLoading(loading));
  const actionSetLogin = (login) => dispatch(setLogin(login));

  useEffect(() => {
    actionSetLoading(true);
    getBookData(userData.tokens.access.token);
    const willFocusSubscription = navigation.addListener('focus', () => {
      getBookData(userData.tokens.access.token);
    });

    return willFocusSubscription;
  }, []);

  const header = (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.helloText}>
          Hello,
          <Text style={styles.nameText}>{userData.user.name}</Text>
        </Text>
        <Button
          color={secondaryColor}
          icon="logout"
          mode="contained"
          onPress={() => {
            actionSetLogin(false);
          }}
          style={{ marginBottom: 10 }}
        >
          Logout
        </Button>
      </View>

      <Title text="Recommended" style={styles.title} />
      <BookCarousel
        data={recommendedData}
        navigation={navigation}
        direction="Detail"
      />
      <Title text="Popular Book" style={styles.title} />
    </>
  );
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LottieView
            source={LoadingImage}
            style={styles.loadingImage}
            autoPlay
            loop
          />
        </View>
      ) : (
        <FlatList
          contentContainerStyle={styles.bookList}
          onRefresh={() => getBookData(userData.tokens.access.token)}
          refreshing={isLoading}
          data={bookData}
          keyExtractor={(item) => item.id}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <BookCard
              navigation={navigation}
              direction="Detail"
              key={item.id}
              data={item}
              action={() => {
                navigation.navigate('Detail', { id: item.id });
                actionSetLoading(true);
              }}
            />
          )}
          ListHeaderComponent={header}
        />
      )}
    </View>
  );
}

export default Home;
