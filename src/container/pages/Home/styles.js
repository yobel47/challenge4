import { StyleSheet, Dimensions } from 'react-native';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loadingContainer: {
    width: window.width,
    height: window.height,
    justifyContent: 'center',
  },
  loadingImage: {
    width: window.width,
  },
  headerContainer: {
    backgroundColor: primaryColor,
    borderBottomLeftRadius: 150,
    borderBottomRightRadius: 150,
    overflow: 'hidden',
    alignItems: 'center',
  },
  helloText: {
    fontFamily: 'Poppins-ExtraBold',
    fontSize: 28,
    textAlign: 'center',
    color: 'white',
    paddingTop: 10,
  },
  nameText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 24,
  },
  title: {
    marginHorizontal: 36,
    marginVertical: 12,
  },
  bookList: {
    alignContent: 'center',
  },
});

export default styles;
