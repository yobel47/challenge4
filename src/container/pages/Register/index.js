import {
  Text,
  View,
  ScrollView,
  StatusBar,
} from 'react-native';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  TextInput,
  Button,
  HelperText,
  Portal,
  Dialog,
} from 'react-native-paper';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { FailedImage, LoadingImage } from '../../assets';
import styles from './styles';
import { primaryColor } from '../../utils/colors';
import {
  DividerLine, HeaderImage, TextLink, Title,
} from '../../components';
import { checkRegister, setAlert } from '../../../redux/actions';

function Register({ navigation }) {
  const [passwordVisible, setPasswordVisible] = useState(true);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [nameCorrect, setNameCorrect] = useState(false);
  const [emailCorrect, setEmailCorrect] = useState(false);
  const [passwordCorrect, setPasswordCorrect] = useState(false);
  const [inputNameColor, setInputNameColor] = useState('');
  const [inputEmailColor, setInputEmailColor] = useState('');
  const [inputPasswordColor, setInputPasswordColor] = useState('');

  const isLoading = useSelector((state) => state.app.isLoading);
  const showAlert = useSelector((state) => state.app.showAlert);
  const errorMessage = useSelector((state) => state.app.alertMessage);

  const dispatch = useDispatch();
  const register = (payload, registerNav) => dispatch(checkRegister(payload, registerNav));
  const showAlertDialog = (visible) => dispatch(setAlert(visible));

  const hideDialog = () => showAlertDialog(false);

  const nameValidate = (nameInput) => {
    setName(nameInput);
    if (nameInput.length > 0) {
      setNameCorrect(false);
      setInputNameColor(primaryColor);
    } else {
      setNameCorrect(true);
      setInputNameColor('red');
    }
  };

  const emailValidate = (emailInput) => {
    setEmail(emailInput);
    const reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(emailInput) === false) {
      setEmailCorrect(true);
      setInputEmailColor('red');
    } else {
      setEmailCorrect(false);
      setInputEmailColor(primaryColor);
    }
  };

  const passwordValidate = (passwordInput) => {
    setPassword(passwordInput);
    const reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
    if (passwordInput.length < 8) {
      setPasswordCorrect(true);
      setInputPasswordColor('red');
    } else if (reg.test(passwordInput) === false) {
      setPasswordCorrect(true);
      setInputPasswordColor('red');
    } else {
      setPasswordCorrect(false);
      setInputPasswordColor(primaryColor);
    }
  };

  const checkRegisterInput = (payload) => {
    if (!nameCorrect) {
      if (!emailCorrect) {
        if (!passwordCorrect) {
          if (email === '' && password === '' && name === '') {
            setNameCorrect(true);
            setInputNameColor('red');
            setEmailCorrect(true);
            setInputEmailColor('red');
            setPasswordCorrect(true);
            setInputPasswordColor('red');
          } else {
            register(payload, navigation);
          }
        } else {
          setPasswordCorrect(true);
          setInputPasswordColor('red');
        }
      } else {
        setEmailCorrect(true);
        setInputEmailColor('red');
      }
    } else {
      setNameCorrect(true);
      setInputNameColor('red');
    }
  };

  return (
    <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LottieView
            source={LoadingImage}
            style={styles.loadingImage}
            autoPlay
            loop
          />
        </View>
      ) : (
        <>
          <View style={styles.containerContent}>
            <HeaderImage type="register" />
            <DividerLine borderStyle={styles.dividerLine} />
            <View style={styles.formContainer}>
              <Title text="Register" />
              <TextInput
                placeholder="Name"
                mode="outlined"
                label="Name"
                style={styles.textInput}
                value={name}
                onChangeText={(text) => nameValidate(text)}
                outlineColor={inputNameColor}
                activeOutlineColor={inputNameColor}
              />
              <HelperText
                type="error"
                visible={nameCorrect}
                style={{ marginBottom: -10 }}
              >
                Name is required!
              </HelperText>

              <TextInput
                placeholder="Email"
                mode="outlined"
                label="Email"
                style={styles.textInput}
                value={email}
                onChangeText={(text) => emailValidate(text)}
                outlineColor={inputEmailColor}
                activeOutlineColor={inputEmailColor}
              />
              <HelperText
                type="error"
                visible={emailCorrect}
                style={{ marginBottom: -10 }}
              >
                Email address is invalid!
              </HelperText>

              <TextInput
                placeholder="Password"
                mode="outlined"
                label="Password"
                style={styles.textInput}
                value={password}
                onChangeText={(text) => passwordValidate(text)}
                secureTextEntry={passwordVisible}
                outlineColor={inputPasswordColor}
                activeOutlineColor={inputPasswordColor}
                right={(
                  <TextInput.Icon
                    name={passwordVisible ? 'eye' : 'eye-off'}
                    onPress={() => setPasswordVisible(!passwordVisible)}
                  />
                )}
              />
              <HelperText
                type="error"
                visible={passwordCorrect}
                style={{ marginBottom: -10 }}
              >
                Password must be at least contain 8 characters, 1 letter, 1
                number!
              </HelperText>

              <Button
                mode="contained"
                style={styles.regButton}
                onPress={() => checkRegisterInput({
                  email,
                  password,
                  name,
                })}
              >
                Register
              </Button>
              <Text style={styles.footerText}>Already have an account?</Text>
              <TextLink
                navigation={navigation}
                direction="MainApp"
                text="Login"
              />
            </View>
          </View>
          <View style={styles.backButton}>
            <Icon.Button
              name="md-caret-back"
              backgroundColor={primaryColor}
              onPress={() => navigation.navigate('MainApp')}
            />
          </View>
        </>
      )}
      <Portal>
        <Dialog visible={showAlert} dismissable={false}>
          <Dialog.Title style={styles.dialogTitle}>
            <Text>Failed</Text>
          </Dialog.Title>
          <Dialog.Content style={styles.dialogContentContainer}>
            <LottieView
              source={FailedImage}
              loop={false}
              autoPlay
              style={styles.dialogLogo}
              resizeMode="cover"
            />
            <Text style={styles.dialogContentText}>
              {errorMessage}
              !
            </Text>
            <Button
              mode="contained"
              style={styles.loginButton}
              onPress={() => hideDialog()}
            >
              Close
            </Button>
          </Dialog.Content>

          <View style={styles.closeAlertContainer}>
            <View style={styles.closeButton}>
              <Button
                icon="close"
                mode="contained"
                style={{
                  marginLeft: 16,
                }}
                onPress={() => hideDialog()}
              />
            </View>
          </View>
        </Dialog>
      </Portal>
    </ScrollView>
  );
}

export default Register;
