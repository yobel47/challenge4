import { StyleSheet, Dimensions } from 'react-native';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loadingContainer: {
    width: window.width,
    height: window.height * 0.94,
    justifyContent: 'center',
  },
  loadingImage: {
    width: window.width,
  },
  contentContainer: {
    flex: 1,
    paddingBottom: 42,
    justifyContent: 'flex-start',
  },
  dividerLine: {
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    overflow: 'hidden',
    marginLeft: -3,
    borderTopWidth: 3,
    borderLeftWidth: 3,
    borderRightWidth: 3,
  },
  formContainer: {
    paddingHorizontal: 36,
    paddingBottom: 24,
  },
  successContainer: {
    alignItems: 'center',
  },
  success: {
    width: window.width * 0.5,
    height: window.width * 0.5,
    marginBottom: -60,
    marginTop: -20,
  },
  loginButton: {
    marginTop: 24,
  },
  footerText: {
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    marginTop: 12,
    fontSize: 16,
  },
  backButton: {
    position: 'absolute',
    backgroundColor: primaryColor,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    overflow: 'hidden',
    marginTop: 10,
    marginLeft: 10,
  },
});

export default styles;
