import NotFound from './404.png';
import LoginImage from './login.svg';
import RegisterImage from './register.svg';
import SuccessImage from './success.json';
import FailedImage from './failed.json';
import EmailCheckImage from './emailCheck.svg';
import BookImage from './book.json';
import LoadingImage from './loading.json';
import BadNetworkImage from './badnetwork.json';

export {
  NotFound,
  LoginImage,
  RegisterImage,
  EmailCheckImage,
  SuccessImage,
  FailedImage,
  BookImage,
  LoadingImage,
  BadNetworkImage,
};
