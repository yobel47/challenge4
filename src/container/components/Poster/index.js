import {
  StyleSheet, View, Image, ActivityIndicator,
} from 'react-native';
import React, { useState } from 'react';
import { NotFound } from '../../assets';

const styles = StyleSheet.create({
  containerItem: {
    borderRadius: 30,
    overflow: 'hidden',
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'stretch',
  },
});

function Poster({
  imgUrl, index, imgSize, style,
}) {
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState({ uri: imgUrl });
  const [styleImg, setStyleImg] = useState([styles.image, { ...imgSize }]);

  const onError = () => {
    setImage(NotFound);
    setStyleImg({
      width: 80,
      height: 80,
      alignSelf: 'center',
    });
  };

  function onLoading(value) {
    setLoading(value);
  }

  return (
    <View style={[styles.containerItem, { ...imgSize }, { ...style }]} key={index}>
      {loading && (
        <View
          style={{
            justifyContent: 'center',
            alignSelf: 'center',
            alignContent: 'center',
            zIndex: 0,
            width: '100%',
            position: 'absolute',
            height: 350,
          }}
        >
          <ActivityIndicator color="grey" size="large" />
        </View>
      )}
      <Image
        source={image}
        style={styleImg}
        onError={onError}
        onLoadStart={() => onLoading(true)}
        onLoadEnd={() => onLoading(false)}
        resizeMode="contain"
      />
    </View>
  );
}

export default Poster;
