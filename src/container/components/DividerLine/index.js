import { StyleSheet, View } from 'react-native';
import React from 'react';
import { primaryColor } from '../../utils/colors';

const styles = StyleSheet.create({
  lineContainer: {
    height: 30,
    width: '102%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderColor: primaryColor,
  },
  line: {
    width: 50,
    height: 5,
    borderRadius: 2,
    backgroundColor: primaryColor,
  },
});

function DividerLine({ borderStyle }) {
  return (
    <View style={[styles.lineContainer, { ...borderStyle }]}>
      <View style={styles.line} />
    </View>
  );
}

export default DividerLine;
