import { StyleSheet, View, Dimensions } from 'react-native';
import React from 'react';
import { RegisterImage, LoginImage, EmailCheckImage } from '../../assets';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  headerImage: {
    paddingTop: 12,
  },
});

function HeaderImage({ type }) {
  const checkType = () => {
    switch (type) {
      case 'register':
        return (
          <RegisterImage height={window.width * 0.6} width={window.width} />
        );
      case 'successRegister':
        return (
          <EmailCheckImage height={window.width * 0.6} width={window.width} />
        );
      case 'login':
        return <LoginImage height={window.width * 0.6} width={window.width} />;
      default:
        return (
          <RegisterImage height={window.width * 0.6} width={window.width} />
        );
    }
  };

  return (
    <View>
      <View style={styles.headerImage}>{checkType(type)}</View>
    </View>
  );
}

export default HeaderImage;
