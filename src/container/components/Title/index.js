import { StyleSheet, Text } from 'react-native';
import React from 'react';

const styles = StyleSheet.create({
  headerText: {
    fontFamily: 'Poppins-Bold',
    fontSize: 32,
    color: 'black',
  },
});

function Title({ text, style }) {
  return <Text style={[styles.headerText, { ...style }]}>{text}</Text>;
}

export default Title;
