import axios from 'axios';
import { REGISTER_SUCCESS, REGISTER_FAILED } from '../types';
import { setLoading, setLogin, setAlert } from './global';

export const successRegister = () => ({
  type: REGISTER_SUCCESS,
});

export const failedRegister = () => ({
  type: REGISTER_FAILED,
});

export const checkRegister = (payload, navigation) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .post('http://code.aldipee.com/api/v1/auth/register', payload)
    .then(() => {
      dispatch(successRegister());
      navigation.replace('SuccessRegister');
      dispatch(setLoading(false));
    })
    .catch((err) => {
      if (err.response.data.message) {
        dispatch(failedRegister());
        dispatch(setLogin(false));
        dispatch(setLoading(false));
        dispatch(setAlert(true, err.response.data.message));
      }
    });
};
