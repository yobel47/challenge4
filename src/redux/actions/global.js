import { SET_LOADING, SET_ALERT, SET_LOGIN } from '../types';

export const setLoading = (loading) => ({
  type: SET_LOADING,
  isLoading: loading,
});

export const setAlert = (visible, message) => ({
  type: SET_ALERT,
  isVisible: visible,
  message,
});

export const setLogin = (login) => ({
  type: SET_LOGIN,
  isLogin: login,
});
