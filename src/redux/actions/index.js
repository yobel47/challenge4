// import axios from 'axios';
// import {
//   SET_LOADING,
//   SET_ALERT,
//   LOGIN_SUCCESS,
//   LOGIN_FAILED,
//   REGISTER_SUCCESS,
//   REGISTER_FAILED,
//   GET_SUCCESS,
//   GET_FAILED,
//   GET_DETAIL_SUCCESS,
//   GET_DETAIL_FAILED,
// } from '../types';

// export const setLoading = loading => ({
//   type: SET_LOADING,
//   isLoading: loading,
// });

// export const setAlert = visible => ({
//   type: SET_ALERT,
//   isVisible: visible,
// });

// export const successLogin = payload => ({
//   type: LOGIN_SUCCESS,
//   payload: payload,
// });

// export const failedLogin = message => ({
//   type: LOGIN_FAILED,
//   message: message,
// });

// export const checkLogin = payload => {
//   return async dispatch => {
//     dispatch(setLoading(true));
//     await axios
//       .post('http://code.aldipee.com/api/v1/auth/login', payload)
//       .then(res => {
//         dispatch(successLogin(res.data));
//         console.log('loginsuccess');
//       })
//       .catch(err => {
//         dispatch(failedLogin(err.response.data.message));
//         console.log('loginfailed');
//       });
//   };
// };

// export const successRegister = () => ({
//   type: REGISTER_SUCCESS,
// });

// export const failedRegister = message => ({
//   type: REGISTER_FAILED,
//   message: message,
// });

// export const checkRegister = (payload, navigation) => {
//   return async dispatch => {
//     dispatch(setLoading(true));
//     await axios
//       .post('http://code.aldipee.com/api/v1/auth/register', payload)
//       .then(res => {
//         dispatch(successRegister());
//         navigation.replace('SuccessRegister');
//         console.log('registersuccess');
//       })
//       .catch(err => {
//         console.log('registerfailed');
//         if (err.response.data.message) {
//           dispatch(failedRegister(err.response.data.message));
//           console.log(err.response.data.message);
//         }
//       });
//   };
// };

// export const successGet = payload => ({
//   type: GET_SUCCESS,
//   payload: payload,
// });

// export const failedGet = message => ({
//   type: GET_FAILED,
//   message: message,
// });

// export const getData = token => {
//   return async dispatch => {
//     dispatch(setLoading(true));
//     await axios
//       .get('http://code.aldipee.com/api/v1/books', {
//         headers: {
//           Authorization: 'Bearer ' + token,
//         },
//       })
//       .then(res => {
//         dispatch(successGet(res.data.results));
//         console.log('booksuccess');
//       })
//       .catch(err => {
//         console.log('bookfailed');
//         if (err.response.data.message) {
//           dispatch(failedGet(err.response.data.message));
//           console.log(err.response.data.message);
//         }
//       });
//   };
// };

// export const successGetDetail = payload => ({
//   type: GET_DETAIL_SUCCESS,
//   payload: payload,
// });

// export const failedGetDetail = message => ({
//   type: GET_DETAIL_FAILED,
//   message: message,
// });

// export const getDataDetail = (id, token) => {
//   return async dispatch => {
//     dispatch(setLoading(true));
//     await axios
//       .get('http://code.aldipee.com/api/v1/books/' + id, {
//         headers: {
//           Authorization: 'Bearer ' + token,
//         },
//       })
//       .then(res => {
//         dispatch(successGetDetail(res.data));
//         console.log('bookdetailsuccess');
//       })
//       .catch(err => {
//         console.log('bookdetailfailed');
//         if (err.response.data.message) {
//           dispatch(failedGetDetail(err.response.data.message));
//           console.log(err.response.data.message);
//         }
//       });
//   };
// };

export * from './global';
export * from './login';
export * from './register';
export * from './getData';
export * from './getDetailData';
