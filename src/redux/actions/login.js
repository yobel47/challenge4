import axios from 'axios';
import { LOGIN_SUCCESS, LOGIN_FAILED } from '../types';
import { setAlert, setLoading, setLogin } from './global';

export const successLogin = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});

export const failedLogin = () => ({
  type: LOGIN_FAILED,
});

export const checkLogin = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .post('http://code.aldipee.com/api/v1/auth/login', payload)
    .then((res) => {
      dispatch(successLogin(res.data));
      dispatch(setLogin(true));
    })
    .catch((err) => {
      dispatch(failedLogin());
      dispatch(setLogin(false));
      dispatch(setLoading(false));
      dispatch(setAlert(true, err.response.data.message));
    });
};
