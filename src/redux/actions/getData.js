import axios from 'axios';
import { GET_SUCCESS, GET_FAILED } from '../types';
import { setLoading, setLogin, setAlert } from './global';

export const successGet = (payload) => ({
  type: GET_SUCCESS,
  payload,
});

export const failedGet = () => ({
  type: GET_FAILED,
});

export const getData = (token) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .get('http://code.aldipee.com/api/v1/books', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      dispatch(successGet(res.data.results));
      dispatch(setLoading(false));
    })
    .catch((err) => {
      if (err.response.data.message) {
        dispatch(failedGet());
        dispatch(setLogin(false));
        dispatch(setLoading(false));
        dispatch(setAlert(true, err.response.data.message));
      }
    });
};
