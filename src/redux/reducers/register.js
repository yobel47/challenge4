import { REGISTER_SUCCESS, REGISTER_FAILED } from '../types';

const initialState = {
  status: false,
};

const RegisterReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return {
        ...state,
        status: true,
      };
    case REGISTER_FAILED:
      return {
        ...state,
        status: false,
      };
    default:
      return state;
  }
};

export default RegisterReducer;
